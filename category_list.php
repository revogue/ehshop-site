<?php include("assets2/theme/header.php") ?>

    <div class="container">

        <div class="header">

            <div class="logo">
                <a href="/"><img src="//dunb17ur4ymx4.cloudfront.net/webstore/logos/809220a289f5f0d5c3359ad65bc55b906aaabd48.png" /></a>
            </div>

            <div class="buttons">
                <div class="toolbar">
                    <div class="logout">

                        <a href="/checkout/logout" class="btn btn-danger">Logout</a>
                    </div>
                    <div class="currency">
                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                            USD <span class="fa fa-caret-down"></span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li >
                                <a href="?currency=AUD">AUD</a>
                            </li>
                            <li >
                                <a href="?currency=BRL">BRL</a>
                            </li>
                            <li >
                                <a href="?currency=CAD">CAD</a>
                            </li>
                            <li >
                                <a href="?currency=DKK">DKK</a>
                            </li>
                            <li >
                                <a href="?currency=EUR">EUR</a>
                            </li>
                            <li >
                                <a href="?currency=NOK">NOK</a>
                            </li>
                            <li >
                                <a href="?currency=NZD">NZD</a>
                            </li>
                            <li >
                                <a href="?currency=PLN">PLN</a>
                            </li>
                            <li >
                                <a href="?currency=GBP">GBP</a>
                            </li>
                            <li >
                                <a href="?currency=SEK">SEK</a>
                            </li>
                            <li class="active">
                                <a href="?currency=USD">USD</a>
                            </li>
                        </ul>
                    </div>
                    <div class="basket">
                        <a href="#" class="btn btn-info" disabled="disabled"><i class="icon-shopping-cart icon-white"></i> 0 item(s) for 0.00 USD</a>
                    </div>
                </div>
            </div>

        </div>

        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">BanterUHC Network</a>
                </div>

                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav">
                        <li class=""><a href="/">Home</a></li>
                        <li class="active"><a href="/category/745620">PERMANENT RANKS</a></li>
                        <li class=""><a href="/category/753614">SUBSCRIPTION ADD-ONS</a></li>
                        <li class=""><a href="/category/767742">COSMETICS</a></li>

                        <li class="visible-xs"><a href="/checkout/logout">Logout</a></li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="notification">

        </div>

        <div class="body">
            <div class="content">

                <div class="panel panel-default">
                    <div class="panel-heading">SUBSCRIPTION ADD-ONS</div>
                    <div class="panel-body">
                        <p>PAID MONTHLY</p>
                    </div>
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">PERMANENT RANKS</div>
                    <div class="panel-body">
                        <div class="category">
                            <div class="packages-image">
                                <div class="package">
                                    <div class="image">
                                        <a href="javascript:void(0);" data-remote="/package/2009827" class="toggle-modal">
                                            <img src="//dunb17ur4ymx4.cloudfront.net/packages/images/b14c963bd5d320ad54269a7e07a58bbad79318c3.png" class="toggle-tooltip img-rounded" title="Click for more details" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="text">
                                            <div class="name">DERP RANK</div>
                                            <div class="price">
                                                10.00 <small>USD</small>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <a href="javascript::void(0);" data-remote="/package/2009827" class="btn btn-sm btn-info btn-block toggle-modal">Buy</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="package">
                                    <div class="image">
                                        <a href="javascript:void(0);" data-remote="/package/2026673" class="toggle-modal">
                                            <img src="//dunb17ur4ymx4.cloudfront.net/packages/images/a217f1a438d11eced062a976f64bbf6c332ef2d2.png" class="toggle-tooltip img-rounded" title="Click for more details" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="text">
                                            <div class="name">MLG RANK</div>
                                            <div class="price">
                                                20.00 <small>USD</small>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <a href="javascript::void(0);" data-remote="/package/2026673" class="btn btn-sm btn-info btn-block toggle-modal">Buy</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="package">
                                    <div class="image">
                                        <a href="javascript:void(0);" data-remote="/package/2026678" class="toggle-modal">
                                            <img src="//dunb17ur4ymx4.cloudfront.net/packages/images/61cbfa4ad0589833615dea9fe7b7679f1ebed2ba.png" class="toggle-tooltip img-rounded" title="Click for more details" />
                                        </a>
                                    </div>
                                    <div class="info">
                                        <div class="text">
                                            <div class="name">TRYHARD RANK</div>
                                            <div class="price">
                                                40.00 <small>USD</small>
                                            </div>
                                        </div>
                                        <div class="button">
                                            <a href="javascript::void(0);" data-remote="/package/2026678" class="btn btn-sm btn-info btn-block toggle-modal">Buy</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include "assets2/theme/sidebar.php"; ?>
        </div>


<?php include("assets2/theme/footer.php") ?>