<?php
    include("assets2/theme/header.php") ;

    $packages = Package::loadAll($_REQUEST['id']);
    $category = new Category($_REQUEST['id']);


?>



    <div class="body">
        <div class="content">

            <?php
            if(!is_null($category->getDescription())):
            ?>
            <div class="panel panel-default">
                 <div class="panel-heading"><?php echo $category->getName() ?></div>
                 <div class="panel-body">
                    <p><?php echo $category->getDescription() ?></p>
                </div>
            </div>
                <?php
            endif;
            ?>
            <div class="panel panel-default">
                <div class="panel-heading">Pacotes</div>
                <div class="panel-body">
                    <div class="category">
                        <div class="packages-row">
                            <table class="table table-hover table-striped">
                                <tbody>

                                <?php

                                foreach ($packages as $package)
                                {
                                    $pontos = strcmp($package->getPriceType(), 'POINT') == 0;

                                    echo '
                                    <tr>
                                        <td class="name">'.$package->getName() .'</td>
                                        <td class="price">
                                            <!--<span class="discount">10.00</span> -->
                                            '. $package->getPrice(). (!$pontos ? ',00' : '' ) .' <small>'. ($pontos ? 'Pontos' : 'Reais') .'</small>
                                        </td>
                                        <td class="button">
                                            <a href="package.php?id='.$package->getIdPackage().'" class="btn btn-sm btn-info btn-block">Ver Info</a>
                                        </td>
                                    </tr>
                                    ';
                                }

                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include "assets2/theme/sidebar.php"; ?>
    </div>


<?php include("assets2/theme/footer.php") ?>