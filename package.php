<?php
include("assets2/theme/header.php");

$package = new Package($_REQUEST['id']);

?>


    <div class="body">
        <div class="content">

            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $package->getName() ?>
                </div>

                <div class="panel-subheading">
                    <div class="row">
                        <div class="col-md-9">
                            <?php $pontos = strcmp($package->getPriceType(), 'POINT') == 0; ?>
                            <h2>Preço - <?php echo $package->getPrice() ?> <?php echo ($pontos ? 'Pontos' : 'Reais')  ?></h2>
                        </div>

                        <div class="col-md-3" style="margin-top: 15px">
                            <a href="checkout2.php?id=<?php echo $package->getIdPackage(); ?>"
                               class="btn btn-lg btn-success btn-block">Comprar</a>
                        </div>
                    </div>

                </div>
                <div class="panel-body">
                    <?php echo $package->getDescription() ?>
                </div>
            </div>

        </div>

        <?php include "assets2/theme/sidebar.php"; ?>
    </div>


<?php include("assets2/theme/footer.php") ?>