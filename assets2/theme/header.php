<?php include('loader.php');


?>

<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Purpleprison | Welcome</title>

    <link href="/assets2/css/style.css" rel="stylesheet">

    <link rel="shortcut icon" href="/assets2/img/favicon.ico">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script type="text/javascript"
            src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js">
    </script>

    <link href="/assets2/css/theme.css" rel="stylesheet">

</head>
<body>

<div class="container">

    <div class="header">

        <div class="logo">
            <a href="/"><img src="http://i.imgur.com/YHsTizC.png" /></a>
        </div>

        <div class="buttons">
            <div class="toolbar">
                <div class="logout">

                    <a href="/checkout/logout" class="btn btn-danger">Sair</a>
                </div>

                <div class="basket">
                    <a href="#" class="btn btn-info" disabled="disabled"><i class="icon-shopping-cart icon-white"></i> 0 item(s) for 0.00 USD</a>
                </div>
            </div>
        </div>

    </div>

    <nav class="navbar navbar-default" role="navigation">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/">Purpleprison</a>
            </div>

            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class=""><a href="/">Home</a></li>

                    <?php
                    $cats = Category::loadAll();

                    foreach ($cats as  $cat)
                    {
                        $subcats =  Category::loadAll($cat->getIdCategory());

                        if(count($subcats) > 0)
                        {
                            echo '<li class="dropdown active">';

                            echo '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'. $cat->getName() .' <i class="fa fa-caret-down"></i></a>';
                            echo '<ul class="dropdown-menu">';

                            foreach ($subcats as $subcat)
                            {
                                echo '<li><a href="/category.php?id='.$subcat->getIdCategory().'">'. $subcat->getName() .'</a></li>';
                            }

                            echo '</ul>';

                            echo '</li>';

                        }
                        else
                        {
                            echo '<li class=""><a href="/category.php?id='.$cat->getIdCategory().'">'. $cat->getName() .'</a></li>';
                        }
                    }
                    ?>
                </ul>
            </div>
        </div>
    </nav>

    <div class="notification">

    </div>