<?php
    include("assets2/theme/header.php");

    if(!isset($_SESSION['logged'])) {
	    ?>

        <div class="body">
            <div class="content">
                <div class="col-md-13">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Por favor, faça login</h3>
                        </div>
                        <div class="panel-body">
                            <h3>Utilize os dados de registro do servidor</h3>
                            <p>&nbsp;</p>
                            <form accept-charset="UTF-8" role="form">
                                <input type="hidden" name="action" value="do_login">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Nick minecraft" name="nick" type="text">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Senha" name="senha" type="password" value="">
                                    </div>

                                    <input class="btn btn-lg btn-success btn-block" type="submit" value="Autenticar">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
		    <?php include "assets2/theme/sidebar.php"; ?>
        </div>


<?php
    }

    include("assets2/theme/footer.php");
?>