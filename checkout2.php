<?php
include( "assets2/theme/header.php" );

$id_package = $_REQUEST['id'];

$package = new Package( $id_package );
?>

    <div class="body">
        <div class="full-width">
            <div class="panel panel-default">
                <div class="panel-heading">Checkout</div>
                <div class="panel-body">
                    <div class="checkout">
                        <form method="post" action="/api.php">

                            <div class="packages">
                                <input type="hidden" name="action" value="create_order"/>
                                <input type="hidden" name="pid" value="<?php echo $id_package ?>"/>

                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>Nome</th>
                                    <th>Preço</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="name"><?php echo $package->getName() ?></td>
                                            <td class="price"><?php echo $package->getPriceFormated() ?> <small><?php echo $package->getPriceTypeName() ?></small></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                            <!--
                            <div class="page-header">
                                <h4>Resgatar cupões / cartões-presente</h4>
                            </div>

                            <div class="coupons">
                                <div class="redeem">
                                    <form method="post" action="/checkout/coupons/add">
                                        <div class="input-group">
                                            <input type="text" name="coupon" placeholder="Tem um código de cupom? Digite aqui e clique em resgatar." class="form-control">
                                            <div class="input-group-btn">
                                                <button class="btn btn-primary">Resgatar <i class="fa fa-arrow-right"></i></button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <div class="redeemed">
                                </div>
                            </div>-->

                            <div class="page-header">
                                <h4>Detalhes do Pagamento</h4>
                            </div>
                            <div class="details">
                                <div class="row">
                                    <div class="name">
                                        <div class="form-group">
                                            <label>Nome Completo:</label>
                                            <input type="input" class="form-control" name="name"/>
                                        </div>
                                    </div>
                                    <div class="email">
                                        <div class="form-group">
                                            <label>Email:</label>
                                            <input type="input" class="form-control" name="email"/>
                                        </div>
                                    </div>
                                    <div class="email">
                                        <div class="form-group">
                                            <label>Data de Nascimento:</label>
                                            <input type="date" class="form-control" name="birthday"/>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="page-header">
                                <h4>Metodo de Pagamento</h4>
                            </div>

                            <div class="gateways">
								<?php
								foreach ( $gateways as $gateway ) {
									if ( ! $gateway->isEnabled() ) {
										continue;
									}

									if ( ! $package->isPoint() && $gateway->usePoint() ) {
										continue;
									}

									?>

                                    <div class="radio">
                                        <label>
                                            <input type="radio" name="gateway" id="gateway"
                                                   value="<?php echo strtolower( $gateway->getName() ) ?>">
                                            <img src="<?php echo $gateway->getURLSignature() ?>"/> <?php echo $gateway->getName() ?>
                                        </label>
                                    </div>

									<?php
								}
								?>
                            </div>


                            <div class="page-header">
                                <h4>Comprar</h4>
                            </div>

                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" value="true" name="agreement">
                                            Eu concordo e aceito com os <a href="javascript::void(0);" data-remote="/terms" class="toggle-modal">termos e condições </a> desta compra.
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-success btn-block"
                                                data-loading-text="Carregando, aguarde...">Comprar &raquo;
                                        </button>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php include( "assets2/theme/footer.php" ) ?>